package Ejercicio;

public enum Persona {
	ALBERTO(Compra.KIWIS),
	JAIME(Compra.MELOCOTONES),
	MAYTE(Compra.SANDIAS),
	JESUS(Compra.KIWIS),
	VICENTE(Compra.PERAS);
	
	private final Compra sucompra;
	// A cada enum de compra es sucompra
	
	//contructor recibimos parametro de la clase Compra
	private Persona(Compra _sucompra) {
	
		this.sucompra = _sucompra;
	}
	public Compra getSucompra() {
		return sucompra;
	}

}
